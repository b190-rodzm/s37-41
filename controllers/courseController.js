const Course = require("../models/Course.js");
const auth = require("../auth.js");
const bcrypt = require("bcrypt");

// ADD COURSE
module.exports.addCourse = (data) => {
	return Course.find( {name: data.name } )
  .then( result => {
		if (result.length > 0){
			return "Course already exists.";
		} else {
			let newCourse = new Course({
				name : data.name,
				description : data.description,
				price : data.price
			});
			return newCourse.save().then((course, error) => {
				if (error) {
					return false;
				} else {
					return true;
				};
			});
		}
	} )
}

// GET ALL COURSES
	module.exports.getAllCourses = () => {
		return Course.find({})
		.then(result=>{
			return result;
		});
	};

// GET ALL ACTIVE COURSES
	module.exports.getAllActive = () => {
		return Course.find({ 'isActive': 'true'})
		.then(res=>{
			return res;
		});
	};
	

	// GET 1 COURSE
	module.exports.getCourse = (reqParams) =>{
		return Course.findById(reqParams.courseId).then(result => {
			return result;
		})
	};
	
	// UPDATE COURSE
	module.exports.updateCourse = (reqParams, reqBody) => {
		let updateCourse = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		};
		return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, err) => {
			if (err) {
				return false;
			} else {
				return updateCourse; 
			}
		})
	};

// DELETE(ARCHIVE) COURSE
module.exports.archiveCourse = (reqParams) => {
	let updateActiveField = { isActive: false };
	return Course.findByIdAndUpdate(reqParams.id, updateActiveField).then((course, err) => {
		if (err) {
			return false;
		} else {
			return course; 
		}
	})
};
