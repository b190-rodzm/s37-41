const User = require("../models/User.js");
const Course = require("../models/Course.js");

const auth = require("../auth.js");
const bcrypt = require("bcrypt");

// REGISTER USER
module.exports.registerUser = (reqBody) => {
	return User.find({email: reqBody.email})
  .then( result => {
		if (result.length > 0){
			return "Email is already registered!";
		} else {
			let newUser = new User(
        {
          firstName: reqBody.firstName,
          lastName: reqBody.lastName,
          email: reqBody.email,
          mobileNo: reqBody.mobileNo,
          password: bcrypt.hashSync(reqBody.password,10)
        }
      );
      return newUser.save()
      .then((user, error) =>{
        if (error) {
          return false;
        } else {
          return "New user successfully registered!";
        };
      });
    }
  }
  )
}

// CHECK IF EMAIL EXISTS
module.exports.checkEmailExists = ( reqBody ) => {
	return User.find( { email: reqBody.email } )
  .then( result => {
		if (result.length > 0){
			return "Email already exists.";
		}else{
			return "Email does not exist.";
		}
	} )
};

// CHECK USER LOGIN
module.exports.loginUser = (req) => {
	return User.findOne({email:req.email}).then(result =>{
		if(result === null){
			return false;
		} else {
			// compareSync = decodes the encrypted password from the database and compares it to the password received from the request body
			// it's a good that if the value returned by a method/function is boolean, the variable name should be answerable by yes/no
			const isPasswordCorrect = bcrypt.compareSync(req.password, result.password);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			}else{
				return false;
			}
		}
	} )
} 

// GET ALL USERS
module.exports.getAllUsers = () => {
  return User.find({})
  .then(result=>{
    return result;
  });
};

// DELETE USER
module.exports.deleteUser =(userId) => {
  return User.findByIdAndRemove(userId)
  .then((result, error) => {
    if(error){
      console.log(error);
      return false;
    }else{
      return "Account deleted."
    }
  })
}

//UPDATE USER INFO
module.exports.updateInfo = (userId, newInfo) => {
  return User.findById(userId)
  .then ((result,error) => {
    if (error){
      console.log(error);
      return false;
    } else {
      result.name = newUser.name;
      return result.save()
      .then((updatedUser, error) => {
        if (error) {
          console.log(error);
          return false
        } else {
          return updatedUser;
        }
      })
    }
  })
};

// GET USER PROFILE
module.exports.getProfile = (data) => {
	console.log(data);
	return User.findById(data.userId).then(result => {
    result.password = "";
		return result;
	});
};

//ENROLL USER
module.exports.enroll = async (data) => {
  let isUserUpdated = await User.findById(data.userId).then(user => {
    user.enrollments.push({courseId: data.courseId});

    return user.save().then((user, error) => {
      if(error){
        return false;
      } else {
        return true;
      }
    })
  });

  let isCourseUpdated = await Course.findById(data.courseId).then(course => {
      course.enrollees.push({userId:data.userId});

      return course.save().then((course, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
      })
    })
    if (isUserUpdated && isCourseUpdated) {
      return true;
    } else {
      return false;
    }
  }