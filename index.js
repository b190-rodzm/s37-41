const express = require("express");
const mongoose = require("mongoose");
const cors = require ("cors");

const app = express();
const userRoutes = require('./routes/userRoutes.js');
const courseRoutes = require('./routes/courseRoutes.js');

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoutes);
app.use("/course", courseRoutes);
app.use(cors());


//MongoDB Connection========================================
mongoose.connect("mongodb+srv://mikerodz:Hydepark@wdc028-course-booking.nlohbhy.mongodb.net/b190-course-booking?retryWrites=true&w=majority",
{useNewUrlParser : true, useUnifiedTopology : true}
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", ()=> console.log("Connected to the database."));

//===========================================================
app.listen(process.env.PORT || 3000, () => console.log(`API now online at port ${process.env.PORT || 3000}.`));