const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController.js");
const auth = require("../auth.js");


// ADD COURSE
router.post("/", auth.verify, (req, res) => {
  let userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin === false) {
		res.send(false);
	} else {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
	}
});

// GET ALL COURSES
router.get('/all', (req, res) => {
  courseController.getAllCourses()
  .then(resultFromController => res.send(resultFromController));
});

// GET ALL ACTIVE COURSES
router.get('/', (req, res) => {
  courseController.getAllActive()
  .then(resultFromController => res.send(resultFromController));
});

//GET COURSE
router.get('/:courseId', (req, res) => {
  courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

//UPDATE COURSE
router.put('/update/:id', auth.verify, (req, res) => {
  courseController.updateCourse(req.params, req.body)
  .then(resultFromController => res.send(resultFromController));
});

//DELETE(ARCHIVE) COURSE
router.put('/archive/:id', auth.verify, (req, res) => {
  courseController.archiveCourse(req.params)
  .then(resultFromController => res.send(resultFromController));
});

module.exports = router;
