const express = require('express');
const userController = require('../controllers/userController');
const auth = require("../auth.js");

const router = express.Router();

//CHECK EMAIL
router.post('/checkEmail', ( req, res ) =>{
	userController.checkEmailExists(req.body)
  .then(resultFromController => res.send(resultFromController));
} )

//REGISTER NEW USER
router.post("/register", (req,res) => { 
  userController.registerUser(req.body)
  .then(resultFromController => res.send(resultFromController));
});

//LOGIN USER
router.post('/login', (req,res) => {
  userController.loginUser(req.body)
  .then(resultFromController => res.send(resultFromController));
});

//GET ALL USERS
router.get("/", (req,res) => {
  userController.getAllUsers()
  .then(resultFromController => res.send (resultFromController));
});

//DELETE USER
router.delete("/:id", (req,res) => {
  userController.deleteUser(req.params.id)
  .then(resultFromController => res.send(resultFromController));
});

// GET PROFILE DETAILS
router.get("/details",auth.verify,(req,res) => { 
  const userData = auth.decode(req.headers.authorization);
  userController.getProfile({userId:userData.id})
  .then(resultFromController => res.send(resultFromController))});

// USER ENROLLMENT
router.post("/enroll", auth.verify,(req,res) => { 
  let data = {
    userId: auth.decode(req.headers.authorization).id,
    courseId: req.body.courseId
  }
  userController.enroll(data)
  .then(resultFromController => res.send(resultFromController));
});


module.exports = router;
